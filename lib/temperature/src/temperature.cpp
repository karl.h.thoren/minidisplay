#include "temperature.h"
#include <ArduinoJson.h>
#include <WiFi.h>
#include <HTTPClient.h>

bool temperature::tryFetchAndParse()
{
    bool station_fetch_ok = false;
    uint32_t t = millis();
    if ((t - lastConnectTime) > (pollInterval * pollConstant))
    {
        Serial.print("connecting to temperature server.");

        WiFiClient client;
        HTTPClient http;
        http.useHTTP10(true);
        http.begin(client, F("http://api.temperatur.nu/tnu_1.17.php?p=bohus&amm&simple&cli=miniDisplay"));

        Serial.print("[HTTP] GET...\n");
        // start connection and send HTTP header
        int httpCode = http.GET();
        if (httpCode > 0)
        {
            // HTTP header has been send and Server response header has been handled
            Serial.printf("[HTTP] GET... code: %d\n", httpCode);

            // file found at server
            if (httpCode == HTTP_CODE_OK)
            {
                StaticJsonDocument<512> doc;
                DeserializationError error = deserializeJson(doc, http.getStream());

                if (error)
                {
                    Serial.print(F("deserializeJson() failed: "));
                    Serial.println(error.c_str());
                }
                else
                {
                    Serial.print("Title: ");
                    Serial.println((const char*)doc["title"]);

                    Serial.print("Exec time: ");
                    Serial.println((float)doc["full_exec_time"]);

                    JsonObject stations_0 = doc["stations"][0];
                    if (stations_0)
                    {
                        stationTitle = (const char*)stations_0["title"];
                        Serial.print("Title: ");
                        Serial.println(stationTitle);
                        if (stations_0.containsKey("temp"))
                        {
                            station_fetch_ok = true;
                            temp = stations_0["temp"];
                            Serial.print("Temp: ");
                            Serial.println(temp, 1);

                            JsonObject stations_0_amm = stations_0["amm"];
                            averageRange = (const char*)stations_0_amm["ammRange"];;
                            Serial.print("AmmRange: ");
                            Serial.println(averageRange);

                            min = stations_0_amm["min"];
                            Serial.print("Temp min: ");
                            Serial.println(min, 1);

                            max = stations_0_amm["max"];
                            Serial.print("Temp max: ");
                            Serial.println(max, 1);

                            maxTime = (const char*)stations_0_amm["maxTime"];
                            Serial.print("MaxTime: ");
                            Serial.println(maxTime);

                            minTime = (const char*)stations_0_amm["minTime"];
                            Serial.print("MinTime: ");
                            Serial.println(minTime);

                            average = stations_0_amm["average"];
                            Serial.print("Temp average: ");
                            Serial.println(average, 1);
                        }
                    }
                }
            }
        }
        else
        {
            Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
        }
        http.end();
        lastConnectTime = t;
    }

    return station_fetch_ok;
}