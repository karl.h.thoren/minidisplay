/**
 * @file temperature.h
 * @author Karl Thorén (karl.h.thoren@gmail.com)
 * @brief Parser of data from temperatur.nu. Designed for single station only.
 * @version 0.2
 * @date 2021-06-26
 * 
 * @copyright Copyright (c) 2021
 * 
 * This lib is using the API from temperatur.nu.
 * API documentation and usage rules is found at
 * http://wiki.temperatur.nu/index.php/API
 */
#ifndef TEMPERATURE_H
#define TEMPERATURE_H

#include <Arduino.h>

/**
 * @brief Parser and data handler.
 * @note Request signing is activated if a signing key is provided.
 */
class temperature
{
private:
    /* Configurations */
    String stationKey;     /**< Station to fetch the data for. */
    String sha1Key;        /**< SHA1 key to be used for signing of the request */
    uint32_t pollInterval; /**< Poll interval time in seconds. */

    /* Internal data */
    uint32_t lastConnectTime;             /**< Last time connection where done. */
    const uint32_t pollConstant = 60000L; /**< Poll calculation constant. */

    /* Returned data */
    String stationTitle; /**< Returned station title. */
    float temp;          /**< Returned station temperature. */
    float max;           /**< Returned station maximum temperature. */
    String maxTime;      /**< Returned station maximum temperature timestamp. */
    float min;           /**< Returned station minimum temperature. */
    String minTime;      /**< Returned station minimum temperature timestamp. */
    float average;       /**< Returned station average temperature. */
    String averageRange; /**< Returned station average temperature range. */

public:
    /**
     * @brief Construct a new temperature object.
     * 
     * @param stationKey Key / name of the station to fetch temperature for.
     * @param sha1Key Sign key.
     * @param pollInterval Poll interval.
     */
    temperature(String stationKey, String sha1Key = "", uint32_t pollInterval = 10u)
    {
        this->stationKey = stationKey;
        this->sha1Key = sha1Key;
        this->pollInterval = pollInterval;
        lastConnectTime = -(pollInterval * pollConstant); // neg on purpose!
        temp = -99.0;
        max = -99.0;
        min = -99.0;
        average = -99.0;
    }

    ~temperature()
    {
    }

    /**
     * @brief Try to fetch and parse data for the station.
     * Data is fetched if the timeout is passed.
     * 
     * @return true Data is fetched from server.
     * @return false Failed to fetch data.
     */
    bool tryFetchAndParse();

    /**
     * @brief Get the Station Title object
     * 
     * @return String& Station title.
     */
    const String & getStationTitle() const
    {
        return stationTitle;
    }

    /**
     * @brief Get the Average Range object
     * 
     * @return String& Average range.
     */
    const String & getAverageRange() const
    {
        return averageRange;
    }

    /**
     * @brief Get the Min Time object
     * 
     * @return String& Min time.
     */
    const String & getMinTime() const
    {
        return minTime;
    }

    /**
     * @brief Get the Max Time object
     * 
     * @return String& Max time.
     */
    const String & getMaxTime() const
    {
        return maxTime;
    }

    /**
     * @brief Get the station temperature
     * 
     * @return float current station temperature.
     */
    float getTemp() const
    {
        return temp;
    }

    /**
     * @brief Get the minimum temperature
     * 
     * @return float current station minimum temperature.
     */
    float getMin() const
    {
        return min;
    }

    /**
     * @brief Get the maximum temperature
     * 
     * @return float current station maximum temperature.
     */
    float getMax() const
    {
        return max;
    }

    /**
     * @brief Get the average temperature
     * 
     * @return float current station average temperature.
     */
    float getAverage() const
    {
        return average;
    }
};

#endif /* TEMPERATURE_H */
