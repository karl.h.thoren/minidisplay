#include <Arduino.h>
#include <WiFi.h>
#include <MQTT.h>
#include <ArduinoJson.h>
#include "mailboxMQTT.h"

namespace mboxmqtt
{
    mailboxMqtt::mailboxMqtt()
        : MQTTClient(2048+1024)
    {
    }

    mailboxMqtt::~mailboxMqtt()
    {
    }

    void mailboxMqtt::begin(MQTTSettings &settings)
    {
        this->settings = settings;
        this->settings.printInfo();
        netH.setInsecure();
        MQTTClient::begin(this->settings.mqttServer.c_str(), this->settings.mqttPort, netH);
        MQTTClient::onMessageAdvanced(mailboxMqtt::staticMsgCallback);
    }

    bool mailboxMqtt::connect()
    {
        bool status = false;
        if (WiFi.status() == WL_CONNECTED)
        {
            Serial.println(F("connecting to mqtt..."));

            if (MQTTClient::connect("miniDisplay", this->settings.mqttUser.c_str(), this->settings.mqttPassword.c_str()))
            {
                Serial.println(F("connected!"));
                if (MQTTClient::subscribe(F("v3/+/devices/+/up")))
                {
                    status = true;
                }
                else
                {
                    mqtt_last_error = MQTTClient::lastError();
                    String err = String(F("Subscribe failed with error ")) + MQTTClient::lastError();
                    Serial.println(err);
                }
            }
            else
            {
                mqtt_last_error = MQTTClient::lastError();
                String err = String(F("Connect failed with error ")) + MQTTClient::lastError();
                Serial.println(err);
            }
        }
        return (status);
    }

    bool mailboxMqtt::loop()
    {
        bool status = true;
        if (MQTTClient::loop())
        {
            // A delay will help base system to work more stable.
            delay(10);
        }
        else
        {
            mqtt_last_error = MQTTClient::lastError();
            String err = String(F("Loop poll failed with error ")) + MQTTClient::lastError();
            Serial.println(err);

            // Not connected, try to connect.
            status = connect();
        }
        return (status);
    }

    void mailboxMqtt::msgCallback(String topic, String message)
    {
        // Use arduinojson.org/v6/assistant to compute the capacity
        DynamicJsonDocument doc(3072);

        Serial.print(F("incoming: "));
        Serial.print(topic);
        Serial.print(F(" - "));
        Serial.println(message);

        DeserializationError err = deserializeJson(doc, message);
        if (err)
        {
            Serial.print(F("deserializeJson() failed with code "));
            Serial.println(err.c_str());
        }
        else
        {
            JsonObject uplink_message = doc["uplink_message"];
            if (uplink_message)
            {
                int port = uplink_message["f_port"] | -1;
                counter = uplink_message["f_cnt"] | -1;
                if (1 == port)
                {
                    // Only port 1 contains valid data.
                    JsonObject decoded_payload = uplink_message["decoded_payload"];
                    if (decoded_payload)
                    {
                        measuredVbat = decoded_payload["analog_in_1"] | 0.0;
                        measuredLetterRaw = decoded_payload["analog_in_2"] | 0.0;
                        measuredSolarPower = decoded_payload["analog_in_4"] | 0.0;
                        letterDetect = decoded_payload["presence_2"] | 0;
                        hatchDetect = decoded_payload["presence_3"] | 0;
                    }
                }

                JsonObject uplink_message_rx_metadata_0 = uplink_message["rx_metadata"][0];
                const char *metadata_time = uplink_message_rx_metadata_0["time"] | "";
                lastMessageTime = metadata_time;
                metadata_rssi = uplink_message_rx_metadata_0["rssi"] | 0;
                metadata_snr = uplink_message_rx_metadata_0["snr"] | 0.0f;

                JsonObject uplink_message_settings = uplink_message["settings"];
                long bandwidth = uplink_message_settings["data_rate"]["lora"]["bandwidth"] | 0;
                int spreading_factor = uplink_message_settings["data_rate"]["lora"]["spreading_factor"] | 0;
                lastDataRate = String(F("SF")) + (unsigned int)spreading_factor + String(F("BW")) + (unsigned long)bandwidth;
            }
            else
            {
                Serial.println(F("No uplink message!"));
            }
        }
    }

    void mailboxMqtt::staticMsgCallback(MQTTClient *client, char topic[], char bytes[], int length)
    {
        mailboxMqtt *mbox = (mailboxMqtt *)(client);

        // create topic string
        String str_topic = String(topic);

        // create payload string
        String str_payload;
        if (bytes != nullptr)
        {
            str_payload = String((const char *)bytes);
        }
        mbox->msgCallback(str_topic, str_payload);
    }

} // namespace mboxmqtt
