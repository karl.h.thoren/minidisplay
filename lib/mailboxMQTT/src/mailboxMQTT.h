/**
 * mailboxMqtt.h
 *
 * Handling of MQTT data published by maildetector TTN integration.
 */
#ifndef MAILBOXMQTT_H
#define MAILBOXMQTT_H

#include <Arduino.h>
#include <WiFi.h>
#include <MQTT.h>
#include <ArduinoJson.h>
#include <WiFiClientSecure.h>

namespace mboxmqtt
{
    class mailboxMqtt;

    class MQTTSettings
    {
    public:
        MQTTSettings()
            : mqttPort(-1){};

        MQTTSettings(MQTTSettings &sett)
        {
            this->mqttServer = sett.mqttServer;
            this->mqttPort = sett.mqttPort;
            this->mqttUser = sett.mqttUser;
            this->mqttPassword = sett.mqttPassword;
        };

        virtual ~MQTTSettings(){};

        void printInfo()
        {
            Serial.print(F("Server: "));
            Serial.println(mqttServer);
            Serial.print(F("Port: "));
            Serial.println(mqttPort);
            Serial.print(F("User: "));
            Serial.println(mqttUser);
            Serial.print(F("password: "));
            Serial.println(mqttPassword);
        }
        void serialize(String &outString)
        {
            const size_t capacity = JSON_OBJECT_SIZE(4) + 1000u;
            StaticJsonDocument<capacity> doc;
            doc["mqttServer"] = this->mqttServer;
            doc["mqttPort"] = this->mqttPort;
            doc["mqttUser"] = this->mqttUser;
            doc["mqttPwd"] = this->mqttPassword;

            serializeJson(doc, outString);
        }

        bool deSerialize(const char buffer[])
        {
            // Use arduinojson.org/v6/assistant to compute the capacity
            const size_t capacity = JSON_OBJECT_SIZE(4) + 1000u;
            StaticJsonDocument<capacity> doc;

            DeserializationError err = deserializeJson(doc, buffer);
            if (err)
            {
                Serial.print(F("deserializeJson() failed with code "));
                Serial.println(err.c_str());
                return false;
            }
            else
            {
                mqttServer = (const char *)doc["mqttServer"];
                mqttPort = doc["mqttPort"];
                mqttUser = (const char *)doc["mqttUser"];
                mqttPassword = (const char *)doc["mqttPwd"];
                return true;
            }
        }

        const String &getMqttServer() const
        {
            return mqttServer;
        }
        int getMqttPort() const
        {
            return mqttPort;
        }
        const String &getMqttUser() const
        {
            return mqttUser;
        }
        const String &getMqttPassword() const
        {
            return mqttPassword;
        }

        void setMqttServer(const String &val)
        {
            mqttServer = val;
        }
        void setMqttPort(int val)
        {
            mqttPort = val;
        }
        void setMqttUser(const String &val)
        {
            mqttUser = val;
        }
        void setMqttPassword(const String &val)
        {
            mqttPassword = val;
        }

    private:
        String mqttServer;
        int mqttPort;
        String mqttUser;
        String mqttPassword;
        friend mailboxMqtt;
    };

    /**
 * @brief MQTT handler for TTN application bohus-letterbox-detector
 *
 */
    class mailboxMqtt : public MQTTClient
    {
    public:
        mailboxMqtt();
        ~mailboxMqtt();
        void begin(MQTTSettings &settings);
        bool connect();
        bool loop();

        float measuredVbat = 500.0f;
        float measuredLetterRaw = 500.0f;
        float measuredSolarPower = 500.0f;
        int letterDetect = 0;
        int hatchDetect = 0;
        String lastMessageTime;
        String lastDataRate;
        int metadata_rssi = 0;
        float metadata_snr = 0;
        int counter = -1;
        lwmqtt_err_t mqtt_last_error = LWMQTT_SUCCESS;

    private:
        void msgCallback(String topic, String message);
        static void staticMsgCallback(MQTTClient *client, char topic[], char bytes[], int length);

        WiFiClientSecure netH;
        MQTTSettings settings;
    };

} // namespace mboxmqtt

#endif // MAILBOXMQTT_H
