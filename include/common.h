/**
 * @file common.h
 * @brief Common defines.
 *
 * @copyright Copyright (c) 2020
 *
 */
#ifndef COMMON_H
#define COMMON_H

#include <Arduino.h>
#include <WebServer.h>
#include <mailboxMQTT.h>
#include <temperature.h>

void handleRoot();
void handleNotFound();
void secTicker();
void setupWebServer();
void setupOta();

void display_setup();
void display_process(const temperature& temp, bool tempFetchOk);
void display_errMsg(const String& message, const String& line2, const String& line3);
void display_otaMsg(const String& line1, const String& line2);

extern WebServer webServer;
extern mboxmqtt::MQTTSettings mqttSettings;
extern mboxmqtt::mailboxMqtt mboxMqtt;
extern temperature temp;

extern const uint BUTTON_PIN;
extern const uint ON_BOARD_LED;

extern String otaMsgL1;
extern String otaMsgL2;
extern String otaMsgL3;

#endif // COMMON_H
