/**
 * @file web.cpp
 * @brief Webserver handler
 *
 * @copyright Copyright (c) 2020
 *
 */
#include <Arduino.h>
#include <WiFi.h>
#include <WebServer.h>
#include <mailboxMQTT.h>
#include <ArduinoOTA.h>
#include <strings_en.h>
#include "common.h"

WebServer webServer(80);

String getHTTPHead(String title)
{
    String page;
    page += FPSTR(HTTP_HEAD_START);
    page.replace(FPSTR(T_v), title);
    page += FPSTR(HTTP_SCRIPT);
    page += FPSTR(HTTP_STYLE);

    {
        String p = FPSTR(HTTP_HEAD_END);
        p.replace(FPSTR(T_c), "invert"); // add class str
        page += p;
    }

    return page;
}

/**
 * @brief Root page of the webserver.
 *
 */
void handleRoot()
{
    String page = getHTTPHead("Minidisplay status");
    String str = FPSTR(HTTP_ROOT_MAIN);
    str.replace(FPSTR(T_t), "Minidisplay status");
    str.replace(FPSTR(T_v), (String)WiFi.getHostname() + " - " + WiFi.localIP().toString());
    page += str;
    page += F("<dl>");
    page += F("<dt>Battery status</dt><dd>");
    page += mboxMqtt.measuredVbat;
    page += F(" v</dd>");
    page += F("<dt>Mail detect status RAW</dt><dd>");
    page += mboxMqtt.measuredLetterRaw;
    page += F("</dd>");
    page += F("<dt>Mail detect status</dt><dd>");
    page += mboxMqtt.letterDetect;
    page += F("</dd>");
    page += F("<dt>Mail hatch status</dt><dd>");
    page += mboxMqtt.hatchDetect;
    page += F("</dd>");
    page += F("<dt>Solar voltage</dt><dd>");
    page += mboxMqtt.measuredSolarPower;
    page += F(" v</dd>");
    page += F("<dt>Counter</dt><dd>");
    page += mboxMqtt.counter;
    page += F("</dd>");
    page += F("<dt>Last message timestamp</dt><dd>");
    page += mboxMqtt.lastMessageTime;
    page += F("</dd>");
    page += F("<dt>Last data rate</dt><dd>");
    page += mboxMqtt.lastDataRate;
    page += F("</dd>");
    page += F("<dt>RSSI</dt><dd>");
    page += mboxMqtt.metadata_rssi;
    page += F("</dd>");
    page += F("<dt>SNR</dt><dd>");
    page += mboxMqtt.metadata_snr;
    page += F("</dd>");
    page += F("<dt>mqtt_last_error</dt><dd>");
    page += mboxMqtt.mqtt_last_error;
    page += F("</dd>");

    page += F("<dt>Temperature station</dt><dd>");
    page += temp.getStationTitle();
    page += F("</dd>");
    page += F("<dt>Temperature</dt><dd>");
    page += temp.getTemp();
    page += F("</dd>");

    page += F("<dt>Min</dt><dd>");
    page += temp.getMin();
    page += F("</dd>");
    page += F("<dt>Min timestamp</dt><dd>");
    page += temp.getMinTime();
    page += F("</dd>");

    page += F("<dt>Average</dt><dd>");
    page += temp.getAverage();
    page += F("</dd>");
    page += F("<dt>Average range</dt><dd>");
    page += temp.getAverageRange();
    page += F("</dd>");

    page += F("<dt>Max</dt><dd>");
    page += temp.getMax();
    page += F("</dd>");
    page += F("<dt>Max timestamp</dt><dd>");
    page += temp.getMaxTime();
    page += F("</dd>");

    page += F("<dt>Chip cores</dt><dd>");
    page += ESP.getChipCores();
    page += F("</dd>");
    page += F("<dt>Chip model</dt><dd>");
    page += ESP.getChipModel();
    page += F("</dd>");
    page += F("<dt>Chip revision</dt><dd>");
    page += ESP.getChipRevision();
    page += F("</dd>");
    page += F("<dt>Flash Size</dt><dd>");
    page += ESP.getFlashChipSize();
    page += F(" bytes</dd>");
    page += F("<dt>Sketch size</dt><dd>");
    page += ESP.getSketchSize();
    page += F("</dd>");
    page += F("<dt>Free sketch space</dt><dd>");
    page += ESP.getFreeSketchSpace();
    page += F("</dd>");
    page += F("<dt>Sdk version</dt><dd>");
    page += ESP.getSdkVersion();
    page += F("</dd>");
    page += F("<dt>Station MAC</dt><dd>");
    page += WiFi.macAddress();
    page += F("</dd>");
    page += F("<dt>Sketch MD5</dt><dd>");
    page += ESP.getSketchMD5();
    page += F("</dd>");
    page += F("<dt>Free heap</dt><dd>");
    page += ESP.getFreeHeap();
    page += F("</dd>");
    page += F("</dl>");
    page += FPSTR(HTTP_END);

    webServer.send(200, FPSTR(HTTP_HEAD_CT), page);
}

void handleNotFound()
{
    String message = "File Not Found\n\n";
    message += "URI: ";
    message += webServer.uri();
    message += "\nMethod: ";
    message += (webServer.method() == HTTP_GET) ? "GET" : "POST";
    message += "\nArguments: ";
    message += webServer.args();
    message += "\n";
    for (uint8_t i = 0; i < webServer.args(); i++)
    {
        message += " " + webServer.argName(i) + ": " + webServer.arg(i) + "\n";
    }
    webServer.send(404, "text/plain", message);
}

void setupWebServer()
{
    webServer.on("/", handleRoot);
    webServer.onNotFound(handleNotFound);
}

String otaMsgL1;
String otaMsgL2;
String otaMsgL3;

void setupOta()
{
    ArduinoOTA.setHostname("miniDisplay");

    // No authentication by default
    //ArduinoOTA.setPassword((const char *)"xxxxx");
    ArduinoOTA.onStart([]() {
        otaMsgL1 = F("OTA Start");
        Serial.println(otaMsgL1);
        display_otaMsg(otaMsgL1, F(""));
    });

    ArduinoOTA.onEnd([]() {
        otaMsgL1 = F("OTA End");
        otaMsgL2 = F("Rebooting...");
        Serial.println(otaMsgL1);
        Serial.println(otaMsgL2);
        display_otaMsg(otaMsgL1, otaMsgL2);
    });

    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
        otaMsgL1 = String(F("OTA: ")) + String(progress / (total / 100)) + String(F("%"));
        otaMsgL2 = F("Download...");
        Serial.println(otaMsgL1);
        display_otaMsg(otaMsgL1, otaMsgL2);
    });

    ArduinoOTA.onError([](ota_error_t error) {
        otaMsgL1 = F("OTA Error");
        otaMsgL2 = String(F("Error[")) + String(error) + String(F("]"));
        if (error == OTA_AUTH_ERROR)
        {
            otaMsgL3 = F("Auth Failed");
        }
        else if (error == OTA_BEGIN_ERROR)
        {
            otaMsgL3 = F("Begin Failed");
        }
        else if (error == OTA_CONNECT_ERROR)
        {
            otaMsgL3 = F("Connect Failed");
        }
        else if (error == OTA_RECEIVE_ERROR)
        {
            otaMsgL3 = F("Receive Failed");
        }
        else if (error == OTA_END_ERROR)
        {
            otaMsgL3 = F("End Failed");
        }
        Serial.println(otaMsgL1);
        Serial.println(otaMsgL2);
        Serial.println(otaMsgL3);
        display_errMsg(otaMsgL1, otaMsgL2, otaMsgL3);
    });
    ArduinoOTA.begin();
}
