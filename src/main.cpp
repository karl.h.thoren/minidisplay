#include <FS.h>          //this needs to be first, or it all crashes and burns...
#include <Arduino.h>
#include <WiFiManager.h> // https://github.com/tzapu/WiFiManager
#include <SPIFFS.h>
#include <ArduinoOTA.h>
#include "common.h"

class IntParameter : public WiFiManagerParameter
{
public:
    IntParameter()
        : WiFiManagerParameter("")
    {
    }

    IntParameter(const char *id, const char *placeholder, long value, const uint8_t length = 10)
        : WiFiManagerParameter("")
    {
        init(id, placeholder, String(value).c_str(), length, "", WFM_LABEL_BEFORE);
    }

    long getValue()
    {
        return String(WiFiManagerParameter::getValue()).toInt();
    }
};

static const uint timeout = 120u; // seconds to run for

const uint BUTTON_PIN = 0u;   // Define pin the button is connected to
const uint ON_BOARD_LED = 2u; // Define pin the on-board LED is connected to

static ulong connectedTimeStamp = 0u;
static ulong connectedTimeStampReconnect = 0u;

mboxmqtt::MQTTSettings mqttSettings;
mboxmqtt::mailboxMqtt mboxMqtt;
const char mqttSettingsFile[] = "/mqttSet.json";
temperature temp("Bohus");

WiFiManager wm; // global wm instance
WiFiManagerParameter mqttServer;
IntParameter         mqttPort;
WiFiManagerParameter mqttUser;
WiFiManagerParameter mqttPwd;
void saveConfigCallback();

void setup()
{
    Serial.begin(115200);
    Serial.println(F("Booting..."));
    display_setup();
    display_errMsg("Booting...", "", "");

    //set LED pin as output
    pinMode(ON_BOARD_LED, OUTPUT);
    digitalWrite(ON_BOARD_LED, HIGH);

    WiFi.mode(WIFI_STA);
    WiFi.hostname(F("miniDisplay"));

    setupOta();
    setupWebServer();

    pinMode(BUTTON_PIN, INPUT);

    if (SPIFFS.begin())
    {
        Serial.println(F("mounted file system"));
        if (SPIFFS.exists(mqttSettingsFile))
        {
            //file exists, reading and loading
            Serial.println(F("reading config file"));
            File configFile = SPIFFS.open(mqttSettingsFile, "r");
            if (configFile)
            {
                Serial.println(F("opened config file"));
                size_t size = configFile.size();
                // Allocate a buffer to store contents of the file.
                std::unique_ptr<char[]> buf(new char[size]);

                configFile.readBytes(buf.get(), size);

                if (mqttSettings.deSerialize(buf.get()))
                {
                    Serial.println(F("parsed json"));
                }
                else
                {
                    Serial.println(F("failed to load json config"));
                }
            }
            else
            {
                Serial.println(F("failed to open config file."));
            }
        }
        else
        {
            Serial.println(F("Config file is not present."));
        }
    }
    else
    {
        Serial.println(F("failed to mount FS"));
    }

    new (&mqttServer) WiFiManagerParameter("mqttServer", "Mailbox MQTT server", mqttSettings.getMqttServer().c_str(), 500u);
    new (&mqttPort) IntParameter("mqttPort", "Mailbox MQTT port", mqttSettings.getMqttPort());
    new (&mqttUser) WiFiManagerParameter("mqttUser", "Mailbox MQTT user", mqttSettings.getMqttUser().c_str(), 500u);
    new (&mqttPwd) WiFiManagerParameter("mqttPwd", "Mailbox MQTT password", mqttSettings.getMqttPassword().c_str(), 500u);


    wm.addParameter(&mqttServer);
    wm.addParameter(&mqttPort);
    wm.addParameter(&mqttUser);
    wm.addParameter(&mqttPwd);
    wm.setSaveParamsCallback(saveConfigCallback);
    wm.setDarkMode(true);

    // set configportal timeout
    wm.setConfigPortalTimeout(timeout);
    bool res;
    res = wm.autoConnect("AutoConnectAP"); // anonymous ap

    if (!res)
    {
        Serial.println("Failed to connect or hit timeout");
        // ESP.restart();
    }
    else
    {
        //if you get here you have connected to the WiFi
        Serial.println("connected...yeey :)");
        Serial.println(F("Connected to WIFI!"));
    }

    connectedTimeStamp = millis();
    connectedTimeStampReconnect = connectedTimeStamp;
}

static bool setupDone = false;
static const ulong CONNECT_FAILED_TIMEOUT_RESTART = 10u * 60u * 1000u;
static const ulong CONNECT_FAILED_TIMEOUT_RECONNECT = 30u * 1000u;

//callback notifying us of the need to save config
void saveConfigCallback()
{
    //save the custom parameters to FS
    Serial.println(F("saving config"));

    String outStr;
    mqttSettings.setMqttServer(mqttServer.getValue());
    mqttSettings.setMqttPort(mqttPort.getValue());
    mqttSettings.setMqttUser(mqttUser.getValue());
    mqttSettings.setMqttPassword(mqttPwd.getValue());
    mqttSettings.printInfo();
    mqttSettings.serialize(outStr);

    File configFile = SPIFFS.open(mqttSettingsFile, "w");
    if (configFile)
    {
        Serial.println(F("Saving data: "));
        Serial.println(outStr);
        configFile.write((const uint8_t *)outStr.c_str(), outStr.length());
        configFile.close();
    }
    else
    {
        Serial.println(F("failed to open config file for writing"));
    }
}

void loop()
{
    // is configuration portal requested?
    if (digitalRead(BUTTON_PIN) == LOW)
    {
        display_errMsg("Portal", "", "");
        // Stop the web server as the config portal needs port 80.
        webServer.stop();

        if (!wm.startConfigPortal("AutoConnectAP"))
        {
            display_errMsg("Restart", "AutoConnectAP", "timeout");
            Serial.println(F("failed to connect and hit timeout"));
            delay(3000);
            ESP.restart();
            delay(5000);
        }

        //if you get here you have connected to the WiFi
        Serial.println(F("Connected to WIFI!"));
        connectedTimeStamp = millis();
        connectedTimeStampReconnect = connectedTimeStamp;
    }

    if (WiFi.isConnected())
    {
        if (!setupDone)
        {
            ArduinoOTA.begin();
            webServer.begin();
            Serial.println(F("Ready"));
            Serial.print(F("IP address: "));
            Serial.println(WiFi.localIP());
            mboxMqtt.begin(mqttSettings);
            mboxMqtt.loop();
            setupDone = true;
        }

        webServer.handleClient();
        ArduinoOTA.handle();
        if (mboxMqtt.connected())
        {
            if (mboxMqtt.loop())
            {
                connectedTimeStamp = millis();
                connectedTimeStampReconnect = connectedTimeStamp;
            }
        }
        else
        {
            /* not connected, retry every 30 sec. */
            if (millis() > (CONNECT_FAILED_TIMEOUT_RECONNECT + connectedTimeStampReconnect))
            {
                mboxMqtt.loop();
                connectedTimeStampReconnect = millis();
            }
        }

        bool tempFetchOk = temp.tryFetchAndParse();
        if (otaMsgL1.isEmpty())
        {
            display_process(temp, tempFetchOk);
        }
        else
        {
            display_errMsg(otaMsgL1, otaMsgL2, otaMsgL3);
        }
    }
    else
    {
        display_errMsg("No WIFI",
                       String(F("Restart ms: ")) + String(millis()),
                       String(F("End: ")) + String(CONNECT_FAILED_TIMEOUT_RESTART + connectedTimeStamp));
        setupDone = false;
        if (millis() > (CONNECT_FAILED_TIMEOUT_RESTART + connectedTimeStamp))
        {
            display_errMsg("Restart", "No WIFI", "");
            Serial.println(F("failed to connect and hit timeout, restart to try again."));
            delay(3000);
            ESP.restart();
            delay(5000);
        }
    }
}
