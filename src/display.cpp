#include <U8g2lib.h>
#include "common.h"

#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif
#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif

// Please UNCOMMENT one of the contructor lines below
// U8g2 Contructor List (Picture Loop Page Buffer)
// The complete list is available here: https://github.com/olikraus/u8g2/wiki/u8g2setupcpp
// Please update the pin numbers according to your setup. Use U8X8_PIN_NONE if the reset pin is not connected
//U8G2_SSD1306_128X64_NONAME_F_HW_I2C u8g2(U8G2_R0, /* reset=*/U8X8_PIN_NONE);
U8G2_SH1106_128X64_NONAME_F_HW_I2C u8g2(U8G2_R0, /* reset=*/ U8X8_PIN_NONE);
// End of constructor list

void display_setup()
{
    u8g2.begin();
    u8g2.enableUTF8Print();
    u8g2.clear();
}

void drawMailAndTemp(bool gotMail, float degree)
{
    if (gotMail)
    {
        u8g2.setFont(u8g2_font_open_iconic_email_2x_t);
        u8g2.drawGlyph(u8g2.getWidth() - u8g2.getMaxCharWidth(), u8g2.getMaxCharHeight(), 64);
        u8g2.setFont(u8g2_font_logisoso38_tf);
    }
    else
    {
        u8g2.setFont(u8g2_font_logisoso42_tf);
    }

    u8g2.setCursor(0, u8g2.getHeight());
    u8g2.print(degree, 1);
}

void display_process(const temperature &temp, bool tempFetchOk)
{
    u8g2.clearBuffer();
    drawMailAndTemp(mboxMqtt.letterDetect, temp.getTemp());
    u8g2.sendBuffer();
}

void display_errMsg(const String &message, const String &line2, const String &line3)
{
    u8g2.clearBuffer();
    u8g2.setFont(u8g2_font_helvR18_tr);
    u8g2.setCursor(0, u8g2.getMaxCharHeight());
    u8g2.print(message);

    u8g2.setFont(u8g2_font_helvR08_tf);
    u8g2.setCursor(0, u8g2.getHeight() - u8g2.getMaxCharHeight() - 2);
    u8g2.print(line2);

    u8g2.setCursor(0, u8g2.getHeight());
    u8g2.print(line3);
    u8g2.sendBuffer();
}

void display_otaMsg(const String& line1, const String& line2)
{
    u8g2.clearBuffer();
    u8g2.setFont(u8g2_font_helvR18_tr);
    u8g2.setCursor(0, u8g2.getMaxCharHeight());
    u8g2.print(line1);

    u8g2.setCursor(0, u8g2.getHeight());
    u8g2.print(line2);
    u8g2.sendBuffer();
}
